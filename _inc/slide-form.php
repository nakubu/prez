<?php
require_once('../config.php');

/* header('Content-Type: application/json; charset=utf-8'); */

$data = array(
    'get' => $_GET,
    'post' => $_POST,
    'files' => $_FILES,
    'server' => $_SERVER,
    'error' => false,
    'status' => ''
);

$data['title'] = 'Success';
$data['status'] = 'Everything is OK!';

$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

if (!$link) {
    $data['title'] = 'Error';
    $data['status'] = 'Could not connect: ' . mysqli_connect_error();
    $data['error'] = true;
    echo json_encode($data);
    exit;
}

$tmp_name = $_FILES['image']['tmp_name'];

$file_dir = __ROOT__.'/upload';
$image_dir = __HOME__.'/upload';

$time = time();
$file_name = $time . '_' . $_FILES['image']['name'];

$file_path = $file_dir . '/' . $file_name;
$image_path = $image_dir . '/' . $file_name;

move_uploaded_file($tmp_name, $file_path);

$image = mysqli_real_escape_string($link, $image_path);
$title = mysqli_real_escape_string($link, $_POST['title']);
$descr = mysqli_real_escape_string($link, $_POST['descr']);

$sql = "INSERT INTO slider (title, descr, image) VALUES ('$title', '$descr', '$image')";

if (!mysqli_query($link, $sql)) {
    $data['title'] = 'Error';
    $data['status'] = mysqli_error($link);
    $data['error'] = true;
    echo json_encode($data);
    exit;
}

$html = '
    <div class="li">
      <div class="image">
        <img src="'.$image.'">
      </div>
      <div class="text">
        <div class="title">'.$title.'</div>
        <div class="descr">'.$descr.'</div>
      </div>
    </div>
';

$data['slide'] = array(
    'image' => $image,
    'title' => $title,
    'descr' => $descr,
    'html' => $html
);

echo json_encode($data);
