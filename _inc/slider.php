<?php
require_once('../config.php');

/* header('Content-Type: application/json; charset=utf-8'); */

$data = array(
    'get' => $_GET,
    'post' => $_POST,
    'files' => $_FILES,
    'server' => $_SERVER,
    'error' => false,
    'status' => ''
);

$data['title'] = 'Success';
$data['status'] = 'Everything is OK!';

$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

if (!$link) {
    $data['title'] = 'Error';
    $data['status'] = 'Could not connect: ' . mysqli_connect_error();
    $data['error'] = true;
    echo json_encode($data);
    exit;
}
$offset = $_GET['index'];
$sql = "SELECT * FROM slider ORDER BY ID DESC LIMIT 1 OFFSET ".$offset;

if (!$result = mysqli_query($link, $sql)) {
    $data['title'] = 'Error';
    $data['status'] = mysqli_error($link);
    $data['error'] = true;
    echo json_encode($data);
    exit;
}

$row = mysqli_fetch_array($result);

$image = $row['image'];
$title = $row['title'];
$descr = $row['descr'];

$html = '
    <div class="li">
      <div class="wrap">
        <div class="content">
          <div class="image">
            <img src="'.$image.'">
          </div>
          <div class="text">
            <div class="title">'.$title.'</div>
            <div class="descr">'.$descr.'</div>
          </div>
        </div>
      </div>
    </div>
';

$data['slide'] = array(
    'image' => $image,
    'title' => $title,
    'descr' => $descr,
    'html' => $html
);

echo json_encode($data);
