<?php require_once('config.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Slider</title>
    <meta content="width=device-width" name="viewport">
    <link href="css/main.css" rel="stylesheet">
    <link href="favicon.ico" rel="icon" type="image/x-icon">
    <script src="js/lib.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>

    <div class="slider slider-plain">

      <div class="list">
        <?php
          $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
          if (!$link) {
            echo 'Could not connect: ' . mysqli_connect_error();
          }
          $result = mysqli_query($link, "SELECT * FROM slider ORDER BY ID DESC");
          $i = 0;
          $act = ' act';
          while($row = mysqli_fetch_array($result)) {
            if ($i > 0) {
              $act = '';
            }
        ?>
            <div class="li<?=$act;?>">
              <div class="wrap">
                <div class="content">
                  <div class="image">
                    <img src="<?=$row['image'];?>">
                  </div>
                  <div class="text">
                    <div class="title"><?=$row['title'];?></div>
                    <div class="descr"><?=$row['descr'];?></div>
                  </div>
                </div>
              </div>
            </div>
        <?php
            $i++;
          }
          mysqli_close($link);
        ?>
      </div>
      <div class="prev">
        <div class="icon"></div>
      </div>
      <div class="next">
        <div class="icon"></div>
      </div>
      <div class="count"></div>
      <div class="effect">
        <select>
          <option value="fade">fade</option>
          <option value="slide">slide</option>
          <option value="slideOver">slideOver</option>
        </select>
      </div>

    </div>

  </body>
</html>
