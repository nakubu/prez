<?php require_once('config.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Slider AJAX</title>
    <meta content="width=device-width" name="viewport">
    <link href="css/main.css" rel="stylesheet">
    <link href="favicon.ico" rel="icon" type="image/x-icon">
    <script src="js/lib.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>

    <?php
      $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
      if (!$link) {
        echo 'Could not connect: ' . mysqli_connect_error();
      }
      $sql = "SELECT * FROM slider ORDER BY ID DESC";
      $result = mysqli_query($link, $sql);
      $total = mysqli_num_rows($result);
      $row = mysqli_fetch_array($result);
      mysqli_close($link);
    ?>

    <div class="slider slider-ajax" data-url="_inc/slider.php" data-index="0" data-total="<?=$total;?>">
      <div class="list">
        <div class="li act">
          <div class="wrap">
            <div class="content">
              <div class="image">
                <img src="<?=$row['image'];?>">
              </div>
              <div class="text">
                <div class="title"><?=$row['title'];?></div>
                <div class="descr"><?=$row['descr'];?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="prev">
        <div class="icon"></div>
      </div>
      <div class="next">
        <div class="icon"></div>
      </div>
      <div class="count">1 / <?=$total;?></div>
      <div class="effect">
        <select>
          <option value="fade">fade</option>
          <option value="slide">slide</option>
          <option value="slideOver">slideOver</option>
        </select>
      </div>

    </div>

  </body>
</html>
