# form
# -------------------------------------
(($)->
    $.fn.form = (o)->
        o = $.extend true,
            ajax: true
            reset: true
            dataType: 'json'
        , o
        this.each ()->
            $form = $ this
            $form.validate
                rules: o.rules
                highlight: (el, errorClass)->
                    $(el).parent().addBack().addClass errorClass
                unhighlight: (el, errorClass)->
                    $(el).parent().addBack().removeClass errorClass
                showErrors: o.showErrors
                errorPlacement: o.errorPlacement or ()->
                submitHandler: o.submitHandler or (form)->
                    if o.ajax
                        $form.ajaxSubmit
                            dataType: o.dataType
                            resetForm: o.reset
                            beforeSubmit: o.beforeSubmit
                            success: o.success
                    else
                        form.submit()
) jQuery



# slideForm
# -------------------------------------
(($)->
    $.fn.slideForm = (o)->
        o = $.extend true,
            speed: 300
            easing: 'easeOutExpo'
        , o
        this.each ()->
            $root = $ this
            $form = $ 'form', $root
            $list = $ '.slide-list'

            init = ()->
                $form.form
                    success: (data)->
                        $item = $ data.slide.html
                        $list.prepend $item.hide().fadeIn o.speed

            init()
) jQuery



# sliderPlain
# -------------------------------------
(($)->
    $.fn.sliderPlain = (o)->
        o = $.extend true,
            speed: 500
            delay: 5000
            easing: 'easeOutExpo'
            effect: 'fade'
        , o
        this.each ()->
            $root = $ this
            $items = $ '.li', $root
            $prev = $ '.prev', $root
            $next = $ '.next', $root
            $count = $ '.count', $root
            $effect = $ '.effect select', $root
            t = null
            busy = false
            total = $items.length
            countSep = ' / '
            currentIndex = $items.index $items.filter '.act'

            return if total < 2

            init = ()->
                $prev.click prev
                $next.click next

                countText = (currentIndex + 1) + countSep + total
                $count.html countText

                $(document).off('.slider').on 'keyup.slider', (e)->
                    switch e.which
                        when 37 then prev()
                        when 39 then next()

                t = setTimeout next, o.delay

            prev = ()->
                index = currentIndex - 1
                show index, 'prev'

            next = ()->
                index = currentIndex + 1
                show index, 'next'

            show = (index, dir)->
                return if busy

                busy = true

                clearTimeout t

                if index < 0
                    index = total - 1
                else if index >= total
                    index = 0

                countText = (index + 1) + countSep + total
                $count.html countText

                effect = $effect.val() or o.effect
                switch effect
                    when 'slide' then slideIn index, dir
                    when 'slideOver' then slideOver index, dir
                    else fadeIn index, dir

            fadeIn = (index, dir)->
                $item = $items.eq index
                $currentItem = $items.eq currentIndex

                currentItemStartCSS =
                    'left': '0%'

                itemStartCSS =
                    'left': '0%'

                $currentItem.css(currentItemStartCSS).fadeOut o.speed, ()->
                    $currentItem.removeClass 'act'

                $item.css(itemStartCSS).fadeIn o.speed, ()->
                    $item.addClass 'act'
                    currentIndex = index
                    busy = false
                    t = setTimeout next, o.delay

            slideIn = (index, dir)->
                $item = $items.eq index
                $currentItem = $items.eq currentIndex

                currentItemEndCSS =
                    'left': '-100%'

                itemStartCSS =
                    'left': '100%'

                itemEndCSS =
                    'left': '0%'

                if dir is 'prev'
                    currentItemEndCSS.left = '100%'
                    itemStartCSS.left = '-100%'

                $currentItem.animate currentItemEndCSS,
                    duration: o.speed
                    easing: o.easing
                    queue: false
                    complete: ()->
                        $currentItem.hide().removeClass 'act'

                $item.css(itemStartCSS).show().animate itemEndCSS,
                    duration: o.speed
                    easing: o.easing
                    queue: false
                    complete: ()->
                        $item.addClass 'act'
                        currentIndex = index
                        busy = false
                        t = setTimeout next, o.delay


            slideOver = (index, dir)->
                $item = $items.eq index
                $currentItem = $items.eq currentIndex

                itemStartCSS =
                    'left': '100%'

                itemEndCSS =
                    'left': '0%'

                if dir is 'prev'
                    itemStartCSS.left = '-100%'

                $item.css(itemStartCSS).show().animate itemEndCSS,
                    duration: o.speed
                    easing: o.easing
                    queue: false
                    complete: ()->
                        $item.addClass 'act'
                        $currentItem.hide().removeClass 'act'
                        currentIndex = index
                        busy = false
                        t = setTimeout next, o.delay

            init()
) jQuery





# sliderAjax
# -------------------------------------
(($)->
    $.fn.sliderAjax = (o)->
        o = $.extend true,
            speed: 500
            delay: 5000
            easing: 'easeOutExpo'
            effect: 'fade'
        , o
        this.each ()->
            $root = $ this
            $list = $ '.list', $root
            $items = $ '.li', $root
            $prev = $ '.prev', $root
            $next = $ '.next', $root
            $count = $ '.count', $root
            $effect = $ '.effect select', $root

            url = $root.data 'url'
            currentIndex = $root.data 'index'
            total = $root.data 'total'

            items = new Array(total);

            countSep = ' / '

            t = null
            busy = false

            return if total < 2

            init = ()->
                items[0] = $('.li', $root).eq 0
                $prev.click prev
                $next.click next

                $(document).off('.slider').on 'keyup.slider', (e)->
                    switch e.which
                        when 37 then prev()
                        when 39 then next()

                t = setTimeout next, o.delay

            prev = ()->
                index = currentIndex - 1

                if index < 0
                    index = total - 1

                return if items[index]
                    show index, 'prev'

                $.getJSON url, index: index, (data)->
                    $item = $ data.slide.html
                    items[index] = $item

                    if index is total - 1
                        $list.append $item
                    else
                        items[currentIndex].before $item

                    show index, 'prev'

            next = ()->
                index = currentIndex + 1

                if index >= total
                    index = 0

                return if items[index]
                    show index, 'next'

                $.getJSON url, index: index, (data)->
                    $item = $ data.slide.html
                    items[index] = $item
                    items[currentIndex].after $item
                    show index, 'next'

            show = (index, dir)->
                return if busy

                busy = true

                clearTimeout t

                if index < 0
                    index = total - 1
                else if index >= total
                    index = 0

                countText = (index + 1) + countSep + total
                $count.html countText

                effect = $effect.val() or o.effect
                switch effect
                    when 'slide' then slideIn index, dir
                    when 'slideOver' then slideOver index, dir
                    else fadeIn index, dir

            fadeIn = (index, dir)->
                $item = items[index]
                $currentItem = items[currentIndex]

                currentItemStartCSS =
                    'left': '0%'

                itemStartCSS =
                    'left': '0%'

                $currentItem.css(currentItemStartCSS).fadeOut o.speed, ()->
                    $currentItem.removeClass 'act'

                $item.css(itemStartCSS).fadeIn o.speed, ()->
                    $item.addClass 'act'
                    currentIndex = index
                    busy = false
                    t = setTimeout next, o.delay

            slideIn = (index, dir)->
                $item = items[index]
                $currentItem = items[currentIndex]

                currentItemEndCSS =
                    'left': '-100%'

                itemStartCSS =
                    'left': '100%'

                itemEndCSS =
                    'left': '0%'

                if dir is 'prev'
                    currentItemEndCSS.left = '100%'
                    itemStartCSS.left = '-100%'

                $currentItem.animate currentItemEndCSS,
                    duration: o.speed
                    easing: o.easing
                    queue: false
                    complete: ()->
                        $currentItem.hide().removeClass 'act'

                $item.css(itemStartCSS).show().animate itemEndCSS,
                    duration: o.speed
                    easing: o.easing
                    queue: false
                    complete: ()->
                        $item.addClass 'act'
                        currentIndex = index
                        busy = false
                        t = setTimeout next, o.delay


            slideOver = (index, dir)->
                $item = items[index]
                $currentItem = items[currentIndex]

                itemStartCSS =
                    'left': '100%'

                itemEndCSS =
                    'left': '0%'

                if dir is 'prev'
                    itemStartCSS.left = '-100%'

                $item.css(itemStartCSS).show().animate itemEndCSS,
                    duration: o.speed
                    easing: o.easing
                    queue: false
                    complete: ()->
                        $item.addClass 'act'
                        $currentItem.hide().removeClass 'act'
                        currentIndex = index
                        busy = false
                        t = setTimeout next, o.delay

            init()
) jQuery



# INIT
# -------------------------------------
$ ()->
    $('.slide-form').slideForm()
    $('.slider-plain').sliderPlain()
    $('.slider-ajax').sliderAjax()

