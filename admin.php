<?php require_once('config.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Slider Admin</title>
    <meta content="width=device-width" name="viewport">
    <link href="css/main.css" rel="stylesheet">
    <link href="favicon.ico" rel="icon" type="image/x-icon">
    <script src="js/lib.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>

    <div class="main">

      <div class="slide-form">
        <h2>New slide</h2>
        <form action="_inc/slide-form.php" enctype="multipart/form-data" method="post">
          <div class="tinput">
            <input class="required" name="title" placeholder="Title" type="text">
          </div>
          <div class="tinput">
            <textarea class="required" name="descr" placeholder="Description"></textarea>
          </div>
          <div class="tinput">
            <input name="image" placeholder="Image" type="file">
          </div>
          <div class="button">
            <button type="submit">Save</button>
          </div>
        </form>
      </div>

      <div class="slide-list">
        <?php
          $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
          if (!$link) {
              echo 'Could not connect: ' . mysqli_connect_error();
          }
          $result = mysqli_query($link, "SELECT * FROM slider ORDER BY ID DESC");
          while($row = mysqli_fetch_array($result)) {
        ?>
            <div class="li">
              <div class="image">
                <img src="<?=$row['image'];?>">
              </div>
              <div class="text">
                <div class="title"><?=$row['title'];?></div>
                <div class="descr"><?=$row['descr'];?></div>
              </div>
            </div>
        <?php
          }
          mysqli_close($link);
        ?>
      </div>

    </div>

  </body>
</html>
